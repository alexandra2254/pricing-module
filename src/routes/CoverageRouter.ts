import Db from "@src/db/database";

import { NextFunction, Request, Response, Router } from "express";
import * as moment from "moment";

const asset = [
    "BTC", "XRP", "BCH", "LTC", "BNB", "ZEC", "XMR", "ETH", "ETC", "USDT", "NEO",
];

function formatDate(date) {
    const monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December",
    ];

    const day = date.getDate();
    const monthIndex = date.getMonth();
    const year = date.getFullYear();

    return monthNames[monthIndex] + " " + day + " " + year;
}

export class CoverageRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.init();
    }

    /*
        Gets 5 most recent trades from any given exchange plus trade pair (selector)
    */
    public async getRecent(req: Request, res: Response, next: NextFunction) {
        if (!req.query.selector) {
            res.status(500)
                .send({
                    message: "Invalid request parameters received",
                    status: res.status,
                });
        } else {
            try {
                const collection = await Db.get("zenbot");

                const trades = await collection.find({ selector: req.query.selector })
                    .sort({ time: -1 })
                    .limit(5)
                    .toArray();

                if (!trades) {
                    res.status(500)
                    .json({
                        message: "No trades found for this selector",
                    });
                }

                trades.forEach((element) => {
                    element.timestamp = new Date ( element.time );
                });

                res.status(200).send(trades);
            } catch (err) {
                res.status(500).send(err);
            }
        }
    }

    /*
        Gets 5 trades closest leading up to datetime provided
        and averages btc price regardless of exchange
    */
    public async getClosestPrice(req: Request, res: Response, next: NextFunction) {
        if ((req.query.datetime).toLowerCase() === "now") {
            req.query.datetime = +new Date();
        }
        const validation = moment.unix(req.query.datetime).isValid();

        if (!req.query.datetime || validation === false) {
            res.status(500)
                .json({
                    message: "Invalid parameters received",
                    status: res.status,
                });
        } else {
            try {
                const collection = await Db.get("zenbot");

                const datetime = parseFloat(req.query.datetime);
                let base;
                let quote;

                // default base and quote set to BTC and USD
                if (!req.get("BASE_ASSET") || !req.get("QUOTE_ASSET")) {
                    base = "BTC";
                    quote = "USD";
                } else {
                    base = (req.get("BASE_ASSET")).toUpperCase();
                    quote = (req.get("QUOTE_ASSET")).toUpperCase();
                }

                const tradePair = base + "-" + quote;
                let regexQuery;

                // Create regex query for different trade pairs and exchanges
                switch (tradePair) {
                    case("BTC-USD"):
                        regexQuery = "(.*BTC-USD)|(.*XXBT-ZUSD)";
                        break;
                    case("XRP-BTC"):
                        regexQuery = "(.*XRP-BTC)|(.*XXRP-XXBT)";
                        break;
                    case("BCH-BTC"):
                        regexQuery = "(.*BCH-BTC)";
                        break;
                    case("LTC-BTC"):
                        regexQuery = "(.*LTC-BTC)|(.*XLTC-XXBT)";
                        break;
                    case("BNB-BTC"):
                        regexQuery = "(.*BNB-BTC)";
                        break;
                    case("ZEC-BTC"):
                        regexQuery = "(.*ZEC-BTC)|(.*XZEC-XXBT)";
                        break;
                    case("XMR-BTC"):
                        regexQuery = "(.*XMR-BTC)|(.*XXMR-XXBT)";
                        break;
                    case("ETH-BTC"):
                        regexQuery = "(.*ETH-BTC)";
                        break;
                    case("ETC-BTC"):
                        regexQuery = "(.*ETC-BTC)|(.*XETC-XXBT)";
                        break;
                    case("BTC-USDT"):
                    case("USDT-BTC"):
                        regexQuery = "(.*BTC-USDT)";
                        break;
                    case("NEO-BTC"):
                        regexQuery = "(.*NEO-BTC)";
                        break;
                    default:
                        break;
                }

                // query 5 trades leading up to datetime given
                const cursor = await collection
                    .find({
                        selector: { $regex: regexQuery },
                        time: { $lte: datetime },
                    })
                    .sort({time: -1})
                    .limit(5);

                if (!cursor) {
                    res.status(500)
                        .json({
                            message: "Trades not found",
                            status: res.status,
                        });
                    return;
                }

                cursor.each((error, doc) => {
                    if (error) {
                        res.status(500)
                            .json({
                                message: "Invalid trade pair",
                                status: res.status,
                            });
                        return;
                    }

                    if (doc === null) {
                        cursor.toArray((err, items) => {
                            let total = 0;
                            items.forEach( (element) => {
                                total += element.price;
                            });
                            const avgPrice = (total / items.length).toFixed(3);
                            items.push({ price: avgPrice + " " + quote });

                            res.status(200).send(items);
                        });
                    }
                });
            } catch (err) {
                res.status(500).send(err);
            }
        }
    }

    /*
        Get trade coverage within range includes # of trades
        TODO: add price of BTC to get USD price of alts
    */
    public async getCoverageInRange(req: Request, res: Response, next: NextFunction) {

        if (!req.query.start || !req.query.asset) {
            res.status(500)
                .send({
                    message: "Invalid request parameters received",
                    status: res.status,
                });
        } else {
            const collection = await Db.get("zenbot");
            const start = +new Date(req.query.start) + 86400000;
            let end;

            if (!req.query.end) {
                end = start + 86400000;
            } else {
                end = +new Date(req.query.end) + 86400000;
            }

            let title;
            switch (req.query.asset) {
                case("BTC"):
                    title = "gdax.BTC-USD";
                    break;
                case("XRP"):
                    title = "poloniex.XRP-BTC";
                    break;
                case("BCH"):
                    title = "binance.BCH-BTC";
                    break;
                case("LTC"):
                    title = "gdax.LTC-BTC";
                    break;
                case("BNB"):
                    title = "binance.BNB-BTC";
                    break;
                case("ZEC"):
                    title = "poloniex.ZEC-BTC";
                    break;
                case("XMR"):
                    title = "poloniex.XMR-BTC";
                    break;
                case("ETH"):
                    title = "gdax.ETH-BTC";
                    break;
                case("ETC"):
                    title = "poloniex.ETC-BTC";
                    break;
                case("USDT"):
                    title = "poloniex.BTC-USDT";
                    break;
                case("NEO"):
                    title = "binance.NEO-BTC";
                    break;
                default:
                    break;
            }

            const cursor = await collection.aggregate([
                {
                    $match: {
                        $or : [{
                            $and : [
                                { selector: title },
                                {
                                    time: {
                                        $gt: start,
                                    },
                                },
                                {
                                    time: {
                                        $lte: end,
                                    },
                                },
                            ],
                        }],
                    },
                },
                {
                    $group: {
                        _id: {
                            $subtract: [
                                { $subtract: [ "$time", 0 ] },
                                { $mod: [
                                    { $subtract: [ "$time", 0 ] },
                                    1000 * 60 * 1440,
                                ]},
                            ],
                        },
                        // selector: "$selector",
                        average_price: { $avg: "$price"},
                        trade_count: { $sum: 1 },
                    },
                },
            ]);

            if (!cursor) {
                res.status(404)
                    .json({
                        message: "Trades not found",
                        status: res.status,
                    });
                return;
            } else {
                cursor.each((error, doc) => {

                    if (error) {
                        res.status(404)
                            .json({
                                message: error,
                                status: res.status,
                            });
                        return;
                    } else if (doc === null) {
                        cursor.toArray((err, items) => {
                            items.forEach( (element) => {
                                element._id = formatDate(new Date(element._id));
                                element.average_price = element.average_price.toFixed(3);
                            });
                            res.status(200).json(items);
                        });
                    }
                });
            }
        }
    }

    /*
        Get coverage per asset per day (not exchange specific) (# of trades)
        TODO: Add in range and price of BTC to get USD price of alts
    */
    public async getCoverageAllExchanges(req: Request, res: Response, next: NextFunction) {

        if (!req.query.date || !req.query.asset) {
            res.status(500)
                .send({
                    message: "Invalid request parameters received",
                    status: res.status,
                });
        } else {
            const collection = await Db.get("zenbot");

            const start = +new Date(req.query.date);
            const end = start + 86400000;

            let selector;
            switch (req.query.asset) {
                case("BTC"):
                    selector = [ /.BTC-USD/, /.XXBT-ZUSD/ ];
                    break;
                case("XRP"):
                    selector = [ /.XRP-BTC/, /.XXRP-XXBT/ ];
                    break;
                case("BCH"):
                    selector = [ /.BCH-BTC/ ];
                    break;
                case("LTC"):
                    selector = [ /.LTC-BTC/ ];
                    break;
                case("BNB"):
                    selector = [ /.BNB-BTC/ ];
                    break;
                case("ZEC"):
                    selector = [ /.BNB-BTC/ ];
                    break;
                case("XMR"):
                    selector = [ /.XMR-BTC/, /.XXMR-XXBT/ ];
                    break;
                case("ETH"):
                    selector = [ /.ETH-BTC/ ];
                    break;
                case("ETC"):
                    selector = [ /.ETC-BTC/, /.XETC-XXBT/ ];
                    break;
                case("USDT"):
                    selector = [ /.BTC-USDT/ ];
                    break;
                case("NEO"):
                    selector = [ /.NEO-BTC/ ];
                    break;
                default:
                    break;
            }

            const cursor = await collection.aggregate([
                {
                    $match: {
                        $or : [{
                            $and : [
                                { selector: { $in: selector } },
                                {
                                    time: {
                                        $gt: start,
                                    },
                                },
                                {
                                    time: {
                                        $lte: end,
                                    },
                                },
                            ],
                        }],
                    },
                },
                {
                    $group: {
                        _id: null,
                        average_price: { $avg: "$price"},
                        trade_count: { $sum: 1 },
                    },
                },
            ]);

            if (!cursor) {
                res.status(404)
                    .json({
                        message: "Trades not found",
                        status: res.status,
                    });
                return;
            } else {
                cursor.each((error, doc) => {

                    if (error) {
                        res.status(404)
                            .json({
                                message: error,
                                status: res.status,
                            });
                        return;
                    } else if (doc === null) {
                        cursor.toArray((err, items) => {
                            items[0]._id = formatDate(new Date(start));
                            items[0].average_price = items[0].average_price.toFixed(3);
                            res.status(200).json(items[0]);
                        });
                    }
                });
            }
        }
    }

    public init() {
        this.router.get("/recent", this.getRecent);
        this.router.get("/price", this.getClosestPrice);
        this.router.get("/range", this.getCoverageInRange);
        this.router.get("/coverage", this.getCoverageAllExchanges);
    }
}

export default new CoverageRouter().router;
