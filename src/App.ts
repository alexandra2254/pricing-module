import * as bodyParser from "body-parser";
import * as dotenv from "dotenv";
import * as express from "express";
import * as logger from "morgan";

import CoverageRouter from "@routes/CoverageRouter";
import Db from "@src/db/database";

class App {
    public express: express.Application;

    constructor() {
        this.express = express();
        this.middleware();
        this.routes();
        dotenv.config();

        const DATABASE_URL = "mongodb://bitbutter:iamsmart@ds113039-a0.mlab.com:13039" +
        ",ds113039-a1.mlab.com:13039/zenbot4?replicaSet=rs-ds113039";

        // Connect to Mlab db on start
        Db.connect("zenbot", DATABASE_URL, (err) => {
            if (err) {
                console.log("Unable to connect to Mongo.");
                process.exit(1);
            } else {
                console.log("finished connecting to mlab db");
            }
        });
    }

    private middleware(): void {
        this.express.use(logger("dev"));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
    }

    private routes(): void {
        this.express.use("/v1/coverage", CoverageRouter);
    }
}

export default new App().express;
