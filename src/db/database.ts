import * as mongodb from "mongodb";

const MongoClient = mongodb.MongoClient;

class Database {
    public state = {
        db: null,
    };

    public cmcState = {
        db: null,
    };

    public connect(db, url, done) {
        if (db === "zenbot") {
            if (this.state.db) {
                return done();
            }

            MongoClient.connect(url, (err, database) => {
                if (err) {
                    return done(err);
                }
                // specific db and collection we are accessing here
                this.state.db = database.db("zenbot4").collection("trades");
                done();
            });
        }
    }

    public get(db) {
        if (db === "zenbot") {
            return this.state.db;
        }
    }

    public close(done) {
        if (this.state.db) {
            this.state.db.close((err, result) => {
                this.state.db = null;
                done(err);
            });
        }
    }
}

const Db = new Database();

export default Db;
