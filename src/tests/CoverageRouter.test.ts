/*
    Tests not completed

    TODO:
    split functions out from router for easier testing
    First test routes then secondly test functions to create returned arrays
*/

import * as chai from "chai";
import chaiHttp = require("chai-http");

import CoverageRouter from "@routes/CoverageRouter";
import {
    prettyPrint,
} from "@src/services/Test";
import app from "src/App";

chai.use(chaiHttp);
const expect = chai.expect;

describe("CoverageRouter", () => {

    describe("GET 5 most recent trades for particular currency pair", () => {

        beforeEach(async () => {
            const requestPath = "/v1/recent";
            const res = await chai.request(app)
                .get(requestPath)
                .set("selector", "gdax.BTC-USD")
                .catch((err) => err.response);
            expect(res.status).to.equal(200);
        });

        it("should get 5 most recent trades and return as array", async () => {
            // expect(data).to.be.an("object");
            // const requestPath = "/v1/assets/sync";
            // const res = await chai.request(app)
            //     .get(requestPath)
            //     .set("BB-MASTER-SECRET", "secret")
            //     .catch((err) => err.response);
            // expect(res.status).to.equal(200);
            // const recentTrades = CoverageRouter.getRecent(data);
            // prettyPrint(recentTrades);
            // expect(recentTrades).to.exist;
        });

    });

    describe("GET /v1/price ", () => {
        it("should retrieve 5 closest trades to datetime and averages BTC price", async () => {
            // expect(syncedAssets.length).to.not.equal(1);
        });
    });

    describe("GET /v1/range", () => {
        it("should get trade coverage within range with # of trades", async () => {
            // expect(syncedAssets.length).to.not.equal(1);
        });
    });

    describe("GET /v1/coverage", () => {
        it("should retrieve trade coverage by day with # of trades", async () => {
            // expect(syncedAssets.length).to.not.equal(1);
        });
    });
});
