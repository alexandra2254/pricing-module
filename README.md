# Module to retrieve price of cryptocurrency

This module utilizes [zenbot](https://github.com/DeviaVir/zenbot) a command-line cryptocurrency trading bot using Node.js and MongoDB. We used zenbot's simulator for backtesting to gather historical data as well as the paper trading mode for updated live market prices. 

Our module provides 5 different functions: 

1) Retrieves 5 most recent trades from any given exchange and currency pair
2) Retrieves 5 closest trades leading up to datetime provided and averages BTC price regardless of exchange
3) Retrieves trade coverage within range includes # of trades
4) Retrieves coverage per asset per day regardless of exchange includes # of trades

Pricing data gathered from zenbot is stored in [MLAB](https://docs.bitbutter.com/).

Sample data stored to MLAB: 

```
  {
    "_id": "5ad3e48013c0b8762b37cbd0",
    "trade_id": 41701698,
    "time": 1523836027492,
    "size": 0.1653,
    "price": 8343.07,
    "side": "sell",
    "id": "gdax.BTC-USD-41701698",
    "selector": "gdax.BTC-USD",
    "timestamp": "2018-04-15T23:47:07.492Z"
  }
```

## Currency pairs and exchanges covered 

Selectors are in the form: 
```
{exchange_slug}.{asset}-{currency}
```

The selectors we chose to use for this module: 
```
binance.ETH-BTC
binance.XRP-BTC 
binance.BCH-BTC
binance.BNB-BTC
binance.LTC-BTC
binance.ZEC-BTC
binance.XMR-BTC
binance.BTC-USDT

gdax.BTC-USD
gdax.ETH-BTC
gdax.LTC-BTC

poloniex.XRP-BTC
poloniex.ETH-BTC
poloniex.LTC-BTC
poloniex.ZEC-BTC
poloniex.XMR-BTC
poloniex.ETC-BTC

kraken.XXBT-ZUSD
kraken.XXRP-XXBT
kraken.XLTC-XXBT
kraken.XZEC-XXBT
kraken.XXMR-XXBT
kraken.XETC-XXBT

bitfinex.BTC-USD

```

## Getting Started

```
yarn
yarn test
yarn start
```

### getRecent 

sample endpoint: 
`/v1/recent?selector=gdax.BTC-USD`

sample response:

```
[
  {
    "_id": "5ad3e48013c0b8762b37cbd0",
    "trade_id": 41701698,
    "time": 1523836027492,
    "size": 0.1653,
    "price": 8343.07,
    "side": "sell",
    "id": "gdax.BTC-USD-41701698",
    "selector": "gdax.BTC-USD",
    "timestamp": "2018-04-15T23:47:07.492Z"
  },
  {
    "_id": "5ad3e48013c0b8762b37cbd1",
    "trade_id": 41701697,
    "time": 1523836027097,
    "size": 0.17687631,
    "price": 8343.07,
    "side": "sell",
    "id": "gdax.BTC-USD-41701697",
    "selector": "gdax.BTC-USD",
    "timestamp": "2018-04-15T23:47:07.097Z"
  },
  {
    "_id": "5ad3e48013c0b8762b37cbd2",
    "trade_id": 41701696,
    "time": 1523836026966,
    "size": 1.06639945,
    "price": 8343.06,
    "side": "buy",
    "id": "gdax.BTC-USD-41701696",
    "selector": "gdax.BTC-USD",
    "timestamp": "2018-04-15T23:47:06.966Z"
  },
  {
    "_id": "5ad3e48013c0b8762b37cbd3",
    "trade_id": 41701695,
    "time": 1523836026966,
    "size": 0.21956,
    "price": 8343.06,
    "side": "buy",
    "id": "gdax.BTC-USD-41701695",
    "selector": "gdax.BTC-USD",
    "timestamp": "2018-04-15T23:47:06.966Z"
  },
  {
    "_id": "5ad3e48013c0b8762b37cbd4",
    "trade_id": 41701694,
    "time": 1523836026966,
    "size": 0.45904055,
    "price": 8343.06,
    "side": "buy",
    "id": "gdax.BTC-USD-41701694",
    "selector": "gdax.BTC-USD",
    "timestamp": "2018-04-15T23:47:06.966Z"
  }
]
```

### getClosestPrice

sample endpoint: 

`/v1/price?datetime={{T}}`


sample response: 

```
[
  {
    "_id": "5ad3e48013c0b8762b37cbd0",
    "trade_id": 41701698,
    "time": 1523836027492,
    "size": 0.1653,
    "price": 8343.07,
    "side": "sell",
    "id": "gdax.BTC-USD-41701698",
    "selector": "gdax.BTC-USD",
    "timestamp": "2018-04-15T23:47:07.492Z"
  },
  {
    "_id": "5ad3e48013c0b8762b37cbd1",
    "trade_id": 41701697,
    "time": 1523836027097,
    "size": 0.17687631,
    "price": 8343.07,
    "side": "sell",
    "id": "gdax.BTC-USD-41701697",
    "selector": "gdax.BTC-USD",
    "timestamp": "2018-04-15T23:47:07.097Z"
  },
  {
    "_id": "5ad3e48013c0b8762b37cbd2",
    "trade_id": 41701696,
    "time": 1523836026966,
    "size": 1.06639945,
    "price": 8343.06,
    "side": "buy",
    "id": "gdax.BTC-USD-41701696",
    "selector": "gdax.BTC-USD",
    "timestamp": "2018-04-15T23:47:06.966Z"
  },
  {
    "_id": "5ad3e48013c0b8762b37cbd3",
    "trade_id": 41701695,
    "time": 1523836026966,
    "size": 0.21956,
    "price": 8343.06,
    "side": "buy",
    "id": "gdax.BTC-USD-41701695",
    "selector": "gdax.BTC-USD",
    "timestamp": "2018-04-15T23:47:06.966Z"
  },
  {
    "_id": "5ad3e48013c0b8762b37cbd4",
    "trade_id": 41701694,
    "time": 1523836026966,
    "size": 0.45904055,
    "price": 8343.06,
    "side": "buy",
    "id": "gdax.BTC-USD-41701694",
    "selector": "gdax.BTC-USD",
    "timestamp": "2018-04-15T23:47:06.966Z"
  }
]
```

### getCoverageInRange

sample endpoint: 

`/v1/range?start=Feb 14 2018&end=Feb 17 2018&asset=BTC`

sample response: 

```
[
  {
    "_id": "February 17 2018",
    "average_price": "10890.474",
    "trade_count": 52978
  },
  {
    "_id": "February 16 2018",
    "average_price": "10693.084",
    "trade_count": 87054
  },
  {
    "_id": "February 15 2018",
    "average_price": "10027.813",
    "trade_count": 74512
  },
  {
    "_id": "February 14 2018",
    "average_price": "9922.249",
    "trade_count": 78319
  }
]
```

### getCoverageAllExchanges

endpoint: 
`/v1/coverage?date=Dec 4 2017&asset=BTC`

sample response:

```
[
  {
    "_id": "December 4 2017",
    "average_price": "10890.474",
    "trade_count": 52978
  },
  {
    "_id": "December 4 2017",
    "average_price": "10693.084",
    "trade_count": 87054
  },
  {
    "_id": "December 4 2017",
    "average_price": "10027.813",
    "trade_count": 74512
  },
  {
    "_id": "December 4 2017",
    "average_price": "9922.249",
    "trade_count": 78319
  }
]
```
